module patientRegister{
        requires javafx.fxml;
        requires transitive javafx.controls;

    opens no.ntnu.idatt.patientRegister to javafx.fxml;
        exports no.ntnu.idatt.patientRegister;
}