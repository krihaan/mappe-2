package no.ntnu.idatt.patientRegister;

/**
 * Interface all the popup dialogs shares
 */
public interface Popup {
    public void create(Patient patient);
}
