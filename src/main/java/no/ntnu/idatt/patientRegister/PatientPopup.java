package no.ntnu.idatt.patientRegister;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.PathElement;

import java.util.Optional;

/**
 * The dialog for adding a patient
 */
public class PatientPopup implements Popup{
    private Dialog<Patient> dialog;
    /**
     * The new patient
     */
    private Patient patient;

    /**
     * Launches and shows the new patient dialog
     */
    public PatientPopup() {
        dialog = new Dialog<>();

        //Fill the dialog
        dialog.setTitle("Add new patient");
        dialog.setHeaderText("Add new patient");
        dialog.setGraphic(new ImageView(this.getClass().getResource("/icons/outline_person_add_black_24dp.png").toString()));

        //Set buttons
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        //Create the pane for the input fields
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150 , 10, 10));

        //Create the inputs
        TextField fName = new TextField();
        fName.setPromptText("First name");

        TextField lName = new TextField();
        lName.setPromptText("Last name");

        TextField secNumber = new TextField();
        secNumber.setPromptText("Social security number");

        TextField diagnosis = new TextField();
        diagnosis.setPromptText("Diagnosis");

        TextField gp = new TextField();
        gp.setPromptText("General practitioner");

        //Create the labels and put everything in the grid
        grid.add(new Label("First Name:"), 0, 0);
        grid.add(fName,1,0);

        grid.add(new Label("Last Name:"), 0, 1);
        grid.add(lName,1,1);

        grid.add(new Label("Social security number:"), 0, 2);
        grid.add(secNumber,1,2);

        grid.add(new Label("Diagnosis:"), 0, 3);
        grid.add(diagnosis,1,3);

        grid.add(new Label("General practitioner:"), 0, 4);
        grid.add(gp,1,4);

        dialog.getDialogPane().setContent(grid);

        //Locks the ok button if the inputs are empty
        Node button = dialog.getDialogPane().lookupButton(ButtonType.OK);
        button.setDisable(true);

        //Eventhandler to enable the ok button
        EventHandler<ActionEvent> textHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                button.setDisable(fName.getText().trim().isEmpty()
                        || lName.getText().trim().isEmpty()
                        || !Patient.isASecNumber(secNumber.getText())
                        || diagnosis.getText().isEmpty()
                        || gp.getText().isEmpty());
                System.out.println("hei");
                actionEvent.consume();
            }
        };

        //Adding the event handler
        fName.setOnAction(textHandler);
        lName.setOnAction(textHandler);
        secNumber.setOnAction(textHandler);
        diagnosis.setOnAction(textHandler);
        gp.setOnAction(textHandler);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == ButtonType.OK) {
                try {
                    return new Patient(fName.getText(), lName.getText(), secNumber.getText(), diagnosis.getText(), gp.getText());
                } catch (Exception e) {
                    e.getMessage();
                }
            }
            return null;
        });

        Optional<Patient> result = dialog.showAndWait();

        if (result.isPresent()) {
            try {
                patient = new Patient(fName.getText(), lName.getText(), secNumber.getText(), diagnosis.getText(), gp.getText());
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    @Override
    public void create(Patient patient) {
        new PatientPopup();
    }

    public Patient getPatient() {
        return patient;
    }
}
