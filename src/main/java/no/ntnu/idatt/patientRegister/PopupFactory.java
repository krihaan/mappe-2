package no.ntnu.idatt.patientRegister;

/**
 * Factory to generate new popup dialogs
 */
public class PopupFactory {
    public Popup getPopup(String popupType, Patient patient) {
        if (popupType.equalsIgnoreCase("ADD")) {
            return new PatientPopup();
        }
        if (popupType.equalsIgnoreCase("EDIT")) {
            return new EditPatientPopup(patient);
        }
        if (popupType.equalsIgnoreCase("DELETE")) {
            return new RemovePopup(patient);
        }
        if (popupType.equalsIgnoreCase("INFO")) {
            return new InfoPopup();
        }


        else return null;
    }
}
