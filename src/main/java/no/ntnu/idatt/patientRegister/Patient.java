package no.ntnu.idatt.patientRegister;


import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple class representing a patient
 */
public class Patient {
    private String firstName;
    private String lastName;
    /**
     * The security number of the patient
     */
    private String secNumber;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * Constructor which only allows valid security numbers
     * @throws Exception if the security numbers is invalid
     */
    public Patient(String firstName, String lastName, String secNumber, String diagnosis, String generalPractitioner) throws Exception {
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
        if (isASecNumber(secNumber)) this.secNumber = secNumber;
        else throw new IllegalArgumentException("Not valid security number");
    }

    /**
     * Validator for security numbers checks that the security number is a string of 11 digits
     *
     * @param secNumber a security number
     * @return true/false if the number is a security number
     */
    public static boolean isASecNumber(String secNumber) {
        secNumber = secNumber.replaceAll("\\s+","");
        Pattern secPattern = Pattern.compile("(\\d{11})");
        Matcher matcher = secPattern.matcher(secNumber);
        return matcher.matches();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * A setter which only allows valid security numbers
     *
     * @param secNumber the new security number value
     */
    public void setSecNumber(String secNumber) {
        if (isASecNumber(secNumber)) this.secNumber = secNumber;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecNumber() {
        return secNumber;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return Objects.equals(firstName, patient.firstName) &&
                Objects.equals(lastName, patient.lastName) &&
                Objects.equals(secNumber, patient.secNumber) &&
                Objects.equals(diagnosis, patient.diagnosis) &&
                Objects.equals(generalPractitioner, patient.generalPractitioner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, secNumber, diagnosis, generalPractitioner);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", secNumber='" + secNumber + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", generalPractitioner='" + generalPractitioner + '\'' +
                '}';
    }
}
