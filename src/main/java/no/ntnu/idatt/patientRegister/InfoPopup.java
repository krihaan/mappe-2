package no.ntnu.idatt.patientRegister;

import javafx.scene.control.Alert;

/**
 * The info dialog box
 */
public class InfoPopup implements Popup{
    /**
     * Method to launch and show dialog
     */
    public InfoPopup() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About");
        String packageName = this.getClass().getPackageName();
        String[] name = packageName.split("\\.");
        alert.setHeaderText(name[2]+"\nVersion: 1");
        alert.setContentText("Created by Kristoffer Aandahl\n Not intended for professional use");

        alert.showAndWait();
    }

    @Override
    public void create(Patient patient) {
        new InfoPopup();
    }
}
