package no.ntnu.idatt.patientRegister;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * The dialog box for deleting a patient
 */
public class RemovePopup implements Popup{
    /**
     * The value to decide if the patient should be removed
     */
    private Optional<ButtonType> result;

    /**
     * Method to launch and show the dialog
     *
     * @param patient the patient to be deleted
     */
    public RemovePopup(Patient patient) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm delete");
        alert.setHeaderText("Are you sure you want to delete "+patient.getFirstName()+" "+patient.getLastName()+"?");
        alert.setContentText("This is non reversible");

        result = alert.showAndWait();
    }
    @Override
    public void create(Patient patient) {
        new RemovePopup(patient);
    }

    /**
     * The result of whether or not the patient should be deleted
     *
     * @return OK/CANCEL if the patient should be deleted
     */
    public ButtonType getResult() {
        return result.get();
    }
}
