package no.ntnu.idatt.patientRegister;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


import java.io.*;

/**
 * The controller for the main fxml object
 */
public class MainController {

    public Button addPatientButton;
    public TableColumn gpColumn;
    public VBox root;
    public AnchorPane statusBar;
    public Label statusLabel;
    @FXML private TableView table;
    /**
     * firstname column
     */
    @FXML private TableColumn fnColumn;
    /**
     * lastname column
     */
    @FXML private TableColumn lnColumn;
    /**
     * social security column
     */
    @FXML private TableColumn numberColumn;
    /**
     * diagnose column
     */
    @FXML private TableColumn dColumn;

    /**
     * The list of all the patients
     */
    ObservableList<Patient> patients = FXCollections.observableArrayList();
    /**
     * A factory to launch popup dialogs
     */
    PopupFactory factory;

    /**
     * The file currently imported
     */
    File selected;

    /**
     * Initializes the register and sets the values for the table columns
     */
    public void initialize() {
        fnColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("firstName"));
        lnColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("lastName"));
        numberColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("secNumber"));
        dColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("diagnosis"));
        gpColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("generalPractitioner"));

        table.setItems(patients);

        factory = new PopupFactory();

        selected = null;
    }

    public void addPatient(ActionEvent actionEvent) {
        PatientPopup patientPopup = (PatientPopup) factory.getPopup("ADD", null);
        Patient patient = patientPopup.getPatient();
        if (patient == null) {
            statusBar.setStyle("-fx-background-color: orangered");
            statusLabel.setText("Status: Patient not added");
        }
        else {
            patients.add(patient);
            table.setItems(patients);
            statusOk();
        }
    }

    public void editPatient(ActionEvent actionEvent) {
        if (getSelectedPatient() == null) {
            statusBar.setStyle("-fx-background-color: red");
            statusLabel.setText("No item selected");
        }
        else {
            statusOk();
            EditPatientPopup editPopup = (EditPatientPopup) factory.getPopup("EDIT", (Patient) getSelectedPatient());
            Patient patient = editPopup.getPatient();
            int index = patients.indexOf(getSelectedPatient());
            patients.remove(getSelectedPatient());
            patients.add(index, patient);
            table.setItems(patients);
        }
    }

    /**
     * Sets the status bar to its natural state
     */
    private void statusOk() {
        statusBar.setStyle("-fx-background-color: darkseagreen");
        statusLabel.setText("Status: OK");

    }

    public void deletePerson(ActionEvent actionEvent) {
        RemovePopup removePopup = (RemovePopup) factory.getPopup("Delete",getSelectedPatient());
        if (removePopup.getResult() == ButtonType.OK){
            patients.remove(getSelectedPatient());
        }
        statusOk();
    }

    public void showInfo(ActionEvent actionEvent) {
        factory.getPopup("INFO",null);
    }

    /**
     * Selects file to read from, and fills the patients list with objects
     */
    public void selectReadFile(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select csv file");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Save file","*.csv"));

        Stage stage = (Stage)root.getScene().getWindow();

        File file = fileChooser.showOpenDialog(stage);
        selected = file;

        try {
            readFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        table.setItems(patients);
    }

    /**
     * Reads a selected csv file, and tries to make Patients out of the rows
     * @param file file to read from
     * @throws IOException
     */
    public void readFile(File file) throws IOException {
        if (file.toString().endsWith(".csv")) {
            BufferedReader reader = null;
            boolean success = false;
            try {
                reader = new BufferedReader(new FileReader(file));
                success = true;
            } catch (IOException e) {
                statusBar.setStyle("-fx-background-color: orangered");
                statusLabel.setText("Status: could not import chosen file");
            }
            if (success) {
                reader.readLine();
                String nextLine = "";

                while ((nextLine = reader.readLine()) != null)
                try {
                    String[] line;
                    line = nextLine.split(";");
                    if (line.length < 5) {
                        patients.add(new Patient(line[0], line[1], line[3], null, line[2]));
                    } else patients.add(new Patient(line[0], line[1], line[3], line[4], line[2]));
                } catch (Exception e) {
                    statusBar.setStyle("-fx-background-color: orangered");
                    statusLabel.setText("Status: Not all could be imported due to invalid security numbers");
                }
            }
        }
        else {
            statusBar.setStyle("-fx-background-color: orangered");
            statusLabel.setText("Status: could not import chosen file");
        }
    }

    /**
     * Selects file path to write to, creates the file and writes all patients to it
     */
    public void selectWriteFile(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select file placement");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(".csv","*.csv"));

        Stage stage = (Stage)root.getScene().getWindow();

        File file = fileChooser.showSaveDialog(stage);
        writeFile(file);
    }

    /**
     * Rewrites the currently selected file to save changes
     */
    public void save(ActionEvent actionEvent) {
        if (selected == null) {
            statusBar.setStyle("-fx-background-color: orangered");
            statusLabel.setText("Status: No file path selected");
        }
        else writeFile(selected);
    }

    /**
     * Writes all patients to a selected csv file
     *
     * @param file path to write too
     */
    public void writeFile(File file) {
        try (PrintWriter printWriter = new PrintWriter(file)) {
            printWriter.write("First name;Last name;General practitioner;Security number;Diagnosis\n");
            for (Patient patient : patients) {
                printWriter.write(patient.getFirstName() + ";"
                        + patient.getLastName() + ";"
                        + patient.getGeneralPractitioner() + ";"
                        + patient.getSecNumber() + ";"
                        + patient.getDiagnosis() + ";\n");
            }
            file.createNewFile();
        }catch (Exception e) {
            statusBar.setStyle("-fx-background-color: orangered");
            statusLabel.setText("Status: could not export to chosen file");
        }
    }

    /**
     * Method to get the patient selected in the table
     *
     * @return selected patient
     */
    public Patient getSelectedPatient() {
        return (Patient) table.getSelectionModel().getSelectedItem();
    }

}
