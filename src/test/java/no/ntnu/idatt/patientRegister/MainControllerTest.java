package no.ntnu.idatt.patientRegister;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class MainControllerTest {
    MainController mainController;
    File file;
    @BeforeEach
    public void Setup() throws Exception {
        mainController = new MainController();
        file = new File("Test.csv");
        mainController.patients.add(new Patient("Test", "Kanin", "11 111 111 111", "Corona", "Dyregod"));
        mainController.patients.add(new Patient("Test", "Hare", "22 222 222 222", "Corona", "Dyregod"));
    }

    @Test
    public void writeFileTest() throws FileNotFoundException {
        mainController.writeFile(file);

        Scanner reader = new Scanner(file);

        Assertions.assertEquals( "Test;Kanin;Dyregod;11 111 111 111;Corona;", reader.nextLine());
        Assertions.assertEquals("Test;Hare;Dyregod;22 222 222 222;Corona;", reader.nextLine());
    }

    @Test
    public void readFileTest() throws Exception {
        mainController.patients.remove(0,2);
        try (PrintWriter writer = new PrintWriter(file)) {
            writer.write("\nTest;Kanin;Dyregod;11 111 111 111;Corona;\nTest;Hare;Dyregod;22 222 222 222;Corona;");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ObservableList<Patient> expected =  FXCollections.observableArrayList();
        expected.add(new Patient("Test", "Kanin", "11 111 111 111", "Corona", "Dyregod"));
        expected.add(new Patient("Test", "Hare", "22 222 222 222", "Corona", "Dyregod"));

        mainController.readFile(file);

        Assertions.assertEquals(expected, mainController.patients);
    }
}
