package no.ntnu.idatt.patientRegister;

import org.junit.jupiter.api.*;

class PatientTest {
    Patient patient;
    @BeforeEach
    void setUp() throws Exception {
        patient = new Patient("Test", "Kanin", "11 111 111 111", "Corona", "Dyregod");
    }

    @Nested
    class isASecPattern {
        @Test
        void is_11digits_aSecNumber() {
            Assertions.assertTrue(patient.isASecNumber("12345678911"));
        }

        @Test
        void is_11digitsWitchSpaces_aSecNumber() {
            Assertions.assertTrue(patient.isASecNumber("12 345 678 911"));
        }

        @Test
        void is_10negativeDigits_not_aSecNumber() {
            Assertions.assertFalse(patient.isASecNumber("-3412436542"));
        }

        @Test
        void is_11chars_not_aSecNumber() {
            Assertions.assertFalse(patient.isASecNumber("asdfasdfafd"));
        }

        @Test
        void is_12digits_not_aSecNumber() {
            Assertions.assertFalse(patient.isASecNumber("143123451341"));
        }

        @Test
        void is_24digits_not_aSecNumber() {
            Assertions.assertFalse(patient.isASecNumber("143123451341143123451341"));
        }

        @Test
        void is_10digits_not_aSecNumber() {
            Assertions.assertFalse(patient.isASecNumber("1234123455"));
        }
    }
    @Nested
    class equals {
        @Test
        void is_equalPatients_equal() throws Exception {
            Assertions.assertTrue(patient.equals(new Patient("Test", "Kanin", "11 111 111 111", "Corona", "Dyregod")));
        }
        @Test
        void is_differentPatients_different() throws Exception {
            Assertions.assertFalse(patient.equals(new Patient("Test", "Hare", "11 111 221 111", "Corona", "Dyregod")));
        }
        void is_equalObject_equal() throws Exception {
            Assertions.assertTrue(patient.equals((Object) new Patient("Test", "Kanin", "11 111 111 111", "Corona", "Dyregod")));
        }
        void is_String_equal() {
            patient.equals("Test,Kanin,11 111 111 111,Corona,Dyregod");
        }
    }
}